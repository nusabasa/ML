from fastapi import FastAPI
from pydantic import BaseModel
import tensorflow as tf
from keras.preprocessing.text import Tokenizer
import numpy as np
import pandas as pd
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import random
import json
from fuzzywuzzy import fuzz
import os
import sys
import re
import nltk
import uvicorn
nltk.download('wordnet')
from nltk.corpus import wordnet as wn
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM
from easygoogletranslate import EasyGoogleTranslate
translator = EasyGoogleTranslate()

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, ROOT_DIR)

# ---- chatbot --------

class ChatInput(BaseModel):
    message: str

class ChatOutput(BaseModel):
    response: str

# Stemmer
Fact = StemmerFactory()
Stemmer = Fact.create_stemmer()

app = FastAPI()

model = tf.keras.models.load_model('chatbot_nusa_model.h5')
ERROR_THRESHOLD = 0.25

df_intents = pd.read_csv('df_intents.csv')

with open('data_chat.json') as json_data:
  data_chat = json.load(json_data)

label = [lbl['tag'] for lbl in data_chat["intents"]]

def bag_of_words(s, data_words, show_details=False):
  bag = [0 for _ in range(len(data_words))]

  tokenizer = Tokenizer()
  tokenizer.fit_on_texts([s])
  word_index = tokenizer.word_index

  words = list(word_index.keys())
  s_words = [Stemmer.stem(word.lower()) for word in words]

  for se in s_words:
    for i, w in enumerate(data_words):
        similarity_ratio = fuzz.ratio(w, se)
        if similarity_ratio >= 93:
          bag[i] = 1
          if show_details:
              print("found in bag: %s" % w)

  return np.expand_dims(np.array(bag), axis=0)

def classify(sentence):
  bag = bag_of_words(sentence, df_intents['words'])
  results = model.predict([bag])[0]

  results = [[i,r] for i,r in enumerate(results) if r > ERROR_THRESHOLD]
  results.sort(key=lambda x: x[1], reverse=True)
  
  return_list = []
  for r in results:
    return_list.append((label[r[0]], r[1]))
  return return_list

def response_bot(sentence):
    results = classify(sentence)
    if results:
      if results[0][1] > 0.5:
        while results:
            for i in data_chat['intents']:
                if i['tag'] == results[0][0]:
                    return random.choice(i['responses'])
            results.pop(0)
      else:
        return "Mohon maaf, Nusa tidak mengerti. Silahkan bertanya pertanyaan yang lain."
    else:
        return "Mohon maaf, Nusa tidak mengerti. Silahkan bertanya pertanyaan yang lain."

@app.post("/chatbot/")
def chatbot_nusa(chat_input: ChatInput):
    message = chat_input.message
    response = response_bot(message)
    return ChatOutput(response=response)

@app.get("/update-chatbot-data/")
def update_chatbot_data():
  with open('data_chat.json') as json_data:
    data_chat_update = json.load(json_data)
  
  global data_chat
  data_chat = data_chat_update

  global df_intents
  df_intents = pd.read_csv('df_intents.csv')

  global model
  model = tf.keras.models.load_model('chatbot_nusa_model.h5')

  global label
  label = [lbl['tag'] for lbl in data_chat["intents"]]

  return {"message": "Berhasil melakukan update data chat, label, df_intents, dan model"}

# --------- question generate -----------

with open('qgenerate_data.json') as qjson_data:
  q_data = json.load(qjson_data)

@app.get("/update-json-question/")
def update_question_data():
  with open('qgenerate_data.json') as qjson_data:
    q_data_update = json.load(qjson_data)
  global q_data
  q_data = q_data_update
  return {"message": "Berhasil melakukan update data question"}

qtokenizer = AutoTokenizer.from_pretrained("adirasayidina/t5-small-nsbs")

qmodel = AutoModelForSeq2SeqLM.from_pretrained("adirasayidina/t5-small-nsbs")

@app.get("/update-model-question/")
def update_question_model():
  global qtokenizer
  qtokenizer = AutoTokenizer.from_pretrained("adirasayidina/t5-small-nsbs")
  global qmodel
  qmodel = AutoModelForSeq2SeqLM.from_pretrained("adirasayidina/t5-small-nsbs")

  return {"message": "Berhasil melakukan update model question"}

def get_question(sentence,answer):
  text = "answer: {} context: {}".format(answer,sentence)
  encoding = qtokenizer.encode_plus(text, truncation=True, return_tensors="pt")
  
  outs = qmodel.generate(input_ids=encoding["input_ids"],
                                  attention_mask=encoding["attention_mask"],
                                  num_beams=5,
                                  num_return_sequences=1,
                                  no_repeat_ngram_size=2,
                                  max_length=256)
  dec = [qtokenizer.decode(ids) for ids in outs]
  return dec[0][5:-4].strip()

def get_distractors(syn,word):
    distractors=[]
    word= word.lower()
    orig_word = word
    word = word.replace(" ","_")
    hypernym = syn.hypernyms()
    if len(hypernym) == 0:
        return distractors
    for item in hypernym[0].hyponyms():
      name = item.lemmas()[0].name()
      name = name.replace("_"," ")
      if name == orig_word:
          continue
      name = " ".join(w.capitalize() for w in name.split())
      if name.strip() is not None and name not in distractors:
        name = translator.translate(name, target_language='id')
        distractors.append(name)
        if len(distractors) == 4:
          break
    return distractors[1:]

def get_distractors_jawa(syn,word):
    distractors=[]
    word = word.lower()
    orig_word = word
    word = word.replace(" ","_")
    hypernym = syn.hypernyms()
    if len(hypernym) == 0:
        return distractors
    for item in hypernym[0].hyponyms():
      name = item.lemmas()[0].name()
      name = name.replace("_"," ")
      if name == orig_word:
          continue
      name = " ".join(w.capitalize() for w in name.split())
      if name.strip() is not None and name not in distractors:
          name_translate = translator.translate(name, target_language='jw')
          distractors.append(name_translate)
          if len(distractors) == 4:
            break
    return distractors[1:]

def getQfromText(text):
  sentences = text.split('. ')
  question = []
  answer = []

  for sentence in sentences:
    keywords = re.findall(r'"(.*?)"', sentence, re.DOTALL)
    sentence = sentence.replace("\"","")
    for keyword in keywords:
      if keyword != 'jw':
        ques = get_question(sentence,keyword)
        ques = translator.translate(ques, target_language='id')
        question.append(ques)
        syn = wn.synsets(keyword,'n')
        if syn:
          keyword = translator.translate(keyword, target_language='id')
        answer.append(keyword)

  return dict({'q': question, 'a': answer})

def MulCQ(text):
  sentences = text.split('. ')
  question = []
  for sentence in sentences:
    keywordQ = re.findall(r'"(.*?)"', sentence, re.DOTALL)
    keywords = re.findall(r'"(.*?)"', sentence, re.DOTALL)
    for keyword in keywords:
      if keyword != 'jw':
        sentence = sentence.replace("\"", "")
        ques = get_question(sentence, keyword)
        if any(ext in ques for ext in keywordQ):
          if wn.synsets(keyword.replace(" ", "_")):
            keywordr = keyword.replace(" ", "_")
            syn = wn.synsets(keywordr,'n')
            if syn:
              dist = get_distractors(syn[0],keywordr)
              ans = translator.translate(keyword, target_language='id')
              dist.append(ans)
              random.shuffle(dist)
              ques = translator.translate(ques, target_language='id')
              dic = dict({'q': ques, 'a': ans, 'c': dist})
              question.append(dic)

          else:
            keywordr = translator.translate(keyword, target_language='en')
            keywordr = keywordr.replace(" ", "_")
            syn = wn.synsets(keywordr,'n')
            if syn:
              dist = get_distractors_jawa(syn[0],keywordr)
              dist.append(keyword)
              random.shuffle(dist)
              ques = translator.translate(ques, target_language='id')
              dic = dict({'q': ques, 'a': keyword, 'c': dist})
              question.append(dic)

  return question

import random

def tfQ(text):
  sentences = text.split('. ')
  question = []
  for sentence in sentences:
    keywords = re.findall(r'"(.*?)"', sentence, re.DOTALL)
    for ans in keywords:
      sentence = sentence.replace("\"", "")
      sentence = sentence.replace(ans, "**")
      if wn.synsets(ans.replace(" ", "_")):
        ansyn = ans.replace(" ", "_")
        syn = wn.synsets(ansyn,'n')
        if syn:
          dist = get_distractors(syn[0],ansyn)
          ans = translator.translate(ans, target_language='id')
          dist.append(ans)
          keyword = random.choice(dist)
          answer = True
          if ans != keyword:
            answer = False
          ques = translator.translate(sentence, target_language='id')
          ques = ques.replace("**", keyword)
          dic = dict({'q': ques, 'a': answer})
          question.append(dic)

      else:
        ansyn = translator.translate(ans.strip(), target_language='en')
        ansyn = ansyn.replace(" ", "_")
        syn = wn.synsets(ansyn,'n')
        if syn:
          dist = get_distractors_jawa(syn[0],ansyn)
          dist.append(ans)
          keyword = random.choice(dist)
          answer = True
          if ans != keyword:
            answer = False
            sentence = sentence.replace(ans, keyword)
          ques = translator.translate(sentence, target_language='id')
          dic = dict({'q': ques, 'a': answer})
          question.append(dic)

  return question
   
@app.get("/percakapan-mc-easy/")
def get_mulchoices_percakapan_easy():
   input = '. '.join([str(elem) for elem in q_data['courses'][0]['levels'][0]['contexts']])
   return {"question": MulCQ(input)}

@app.get("/percakapan-e-easy/")
def get_essay_percakapan_easy():
   input = '. '.join([str(elem) for elem in q_data['courses'][0]['levels'][0]['contexts']])
   return {"question": getQfromText(input)}

@app.get("/percakapan-tf-easy/")
def get_tf_percakapan_easy():
   input = '. '.join([str(elem) for elem in q_data['courses'][0]['levels'][0]['contexts']])
   return {"question": tfQ(input)}

@app.get("/percakapan-mc-medium/")
def get_mulchoices_percakapan_medium():
   input = '. '.join([str(elem) for elem in q_data['courses'][0]['levels'][1]['contexts']])
   return {"question": MulCQ(input)}

@app.get("/percakapan-e-medium/")
def get_essay_percakapan_medium():
   input = '. '.join([str(elem) for elem in q_data['courses'][0]['levels'][1]['contexts']])
   return {"question": getQfromText(input)}

@app.get("/percakapan-tf-medium/")
def get_tf_percakapan_medium():
   input = '. '.join([str(elem) for elem in q_data['courses'][0]['levels'][1]['contexts']])
   return {"question": tfQ(input)}

@app.get("/percakapan-mc-hard/")
def get_mulchoices_percakapan_hard():
   input = '. '.join([str(elem) for elem in q_data['courses'][0]['levels'][2]['contexts']])
   return {"question": MulCQ(input)}

@app.get("/percakapan-e-hard/")
def get_essay_percakapan_hard():
   input = '. '.join([str(elem) for elem in q_data['courses'][0]['levels'][2]['contexts']])
   return {"question": getQfromText(input)}

@app.get("/percakapan-tf-hard/")
def get_tf_percakapan_hard():
   input = '. '.join([str(elem) for elem in q_data['courses'][0]['levels'][2]['contexts']])
   return {"question": tfQ(input)}

# KATA SIFAT

@app.get("/katasifat-mc-easy/")
def get_mulchoices_katasifat_easy():
   input = '. '.join([str(elem) for elem in q_data['courses'][1]['levels'][0]['contexts']])
   print(input)
   return {"question": MulCQ(input)}

@app.get("/katasifat-e-easy/")
def get_essay_katasifat_easy():
   input = '. '.join([str(elem) for elem in q_data['courses'][1]['levels'][0]['contexts']])
   return {"question": getQfromText(input)}

@app.get("/katasifat-tf-easy/")
def get_tf_katasifat_easy():
   input = '. '.join([str(elem) for elem in q_data['courses'][1]['levels'][0]['contexts']])
   return {"question": tfQ(input)}

@app.get("/katasifat-mc-medium/")
def get_mulchoices_katasifat_medium():
   input = '. '.join([str(elem) for elem in q_data['courses'][1]['levels'][1]['contexts']])
   return {"question": MulCQ(input)}

@app.get("/katasifat-e-medium/")
def get_essay_katasifat_medium():
   input = '. '.join([str(elem) for elem in q_data['courses'][1]['levels'][1]['contexts']])
   return {"question": getQfromText(input)}

@app.get("/katasifat-tf-medium/")
def get_tf_katasifat_medium():
   input = '. '.join([str(elem) for elem in q_data['courses'][1]['levels'][1]['contexts']])
   return {"question": tfQ(input)}

@app.get("/katasifat-mc-hard/")
def get_mulchoices_katasifat_hard():
   input = '. '.join([str(elem) for elem in q_data['courses'][1]['levels'][2]['contexts']])
   return {"question": MulCQ(input)}

@app.get("/katasifat-e-hard/")
def get_essay_katasifat_hard():
   input = '. '.join([str(elem) for elem in q_data['courses'][1]['levels'][2]['contexts']])
   return {"question": getQfromText(input)}

@app.get("/katasifat-tf-hard/")
def get_tf_katasifat_hard():
   input = '. '.join([str(elem) for elem in q_data['courses'][1]['levels'][2]['contexts']])
   return {"question": tfQ(input)}

# KATA KERJA

@app.get("/katakerja-mc-easy/")
def get_mulchoices_katakerja_easy():
   input = '. '.join([str(elem) for elem in q_data['courses'][2]['levels'][0]['contexts']])
   return {"question": MulCQ(input)}

@app.get("/katakerja-e-easy/")
def get_essay_katakerja_easy():
   input = '. '.join([str(elem) for elem in q_data['courses'][2]['levels'][0]['contexts']])
   return {"question": getQfromText(input)}

@app.get("/katakerja-tf-easy/")
def get_tf_katakerja_easy():
   input = '. '.join([str(elem) for elem in q_data['courses'][2]['levels'][0]['contexts']])
   return {"question": tfQ(input)}

@app.get("/katakerja-mc-medium/")
def get_mulchoices_katakerja_medium():
   input = '. '.join([str(elem) for elem in q_data['courses'][2]['levels'][1]['contexts']])
   return {"question": MulCQ(input)}

@app.get("/katakerja-e-medium/")
def get_essay_katakerja_medium():
   input = '. '.join([str(elem) for elem in q_data['courses'][2]['levels'][1]['contexts']])
   return {"question": getQfromText(input)}

@app.get("/katakerja-tf-medium/")
def get_tf_katakerja_medium():
   input = '. '.join([str(elem) for elem in q_data['courses'][2]['levels'][1]['contexts']])
   return {"question": tfQ(input)}

@app.get("/katakerja-mc-hard/")
def get_mulchoices_katakerja_hard():
   input = '. '.join([str(elem) for elem in q_data['courses'][2]['levels'][2]['contexts']])
   return {"question": MulCQ(input)}

@app.get("/katakerja-e-hard/")
def get_essay_katakerja_hard():
   input = '. '.join([str(elem) for elem in q_data['courses'][2]['levels'][2]['contexts']])
   return {"question": getQfromText(input)}

@app.get("/katakerja-tf-hard/")
def get_tf_katakerja_hard():
   input = '. '.join([str(elem) for elem in q_data['courses'][2]['levels'][2]['contexts']])
   return {"question": tfQ(input)}

@app.get("/translate")
def translate(input: str, lang:str):
   trans = translator.translate(input, target_language=lang)
   return {"message": trans}

if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', port=int(os.environ.get('PORT', 8000)))
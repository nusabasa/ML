## Chatbot API Documentation

The API documentation provides information about the available endpoints and how to interact with the chatbot service.

**Link :** https://nusabasa-chatbot-n6iri666wa-et.a.run.app/

To explore the API, please refer to the [Swagger UI Documentation](https://nusabasa-chatbot-n6iri666wa-et.a.run.app/docs).
